#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>

#define UBRRVAL F_CPU/8/BAUD-1 /* calculates UBRRVAL for BAUD baudrate, BAUD must be defined */

static int uputc(char,FILE*);

static int uputc(char c,FILE *stream)
/* uart writer function for libc stdio functions */
{
	if (c == '\n')
		uputc('\r',stream);
	loop_until_bit_is_set(UCSR0A, UDRE0);	/* wait until we can send a new byte */
	UDR0 = (uint8_t) c;
	return 0;
}

/* define writer function as a stdio stream */
static FILE mystdout = FDEV_SETUP_STREAM(uputc, NULL,_FDEV_SETUP_WRITE);

//uart receive isr
ISR(USART_RX_vect)
{
	uint8_t c = UDR0;
	loop_until_bit_is_set(UCSR0A, UDRE0);	/* wait until we can send a new byte */
	UDR0 = (uint8_t) c;
}

int main(void)
{
	/* tell stdio to use our stream as default */
	stdout=&mystdout;

	/* pin config */
	DDRB = (1 << DDB5);

	/* uart config */
	UCSR0A = (1 << U2X0); /* this sets U2X0 to 1 */
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0);
	UCSR0C = (3 << UCSZ00); /* this sets UCSZ00 to 3 */

	/* baudrate setings (variable set by macros) */
	UBRR0H = (UBRRVAL) >> 8;
	UBRR0L = UBRRVAL;

	/* ADC cfg */
	sei();							  /* enable interrupts */
	puts("hi!");

	for (;;) {
	}
}
